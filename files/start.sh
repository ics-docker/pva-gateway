#!/bin/bash
set -e

# Ensure the following environment variables are set
# Exit script and container if not set
echo "Checking mandatory environment variables..."
test "$PVAGW_CLIENT_ADDRLIST"
test "$PVAGW_STATUSPREFIX"

# Container should have 2 IPs
# Keep the dynamic one (remove the fixed external IP)
ip=$(hostname -i | sed "s/${CONTAINER_EXTERNAL_IP}//" | tr -d '[:space:]')
export PVAGW_SERVER_INTERFACE="${PVAGW_SERVER_INTERFACE:-$ip}"

echo "Create the gw.conf configuration file"
/usr/local/bin/confd -onetime -backend env

echo "Configuration to use"
cat /opt/pvagw/gw.conf

echo "Starting pvagw"
exec pvagw $PVAGW_EXTRA_ARGUMENTS /opt/pvagw/gw.conf
