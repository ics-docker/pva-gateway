FROM registry.esss.lu.se/ics-docker/miniconda:4.8.3

USER root

# Install tools to allow debugging
RUN yum install -y net-tools iproute \
  && yum clean all \
  && rm -rf /var/cache/yum

ENV CONFD_VERSION 0.16.0
RUN curl -L -o /usr/local/bin/confd \
  "https://github.com/kelseyhightower/confd/releases/download/v${CONFD_VERSION}/confd-${CONFD_VERSION}-linux-amd64" \
  && chmod a+x /usr/local/bin/confd

COPY files/confd /etc/confd
COPY files/start.sh /usr/local/bin/start.sh

RUN mkdir -p /opt/pvagw \
  && chown conda:users /opt/pvagw

USER conda

ENV P4P_VERSION=4.0.0
# Keep only ics-conda-forge-virtual channel by default
RUN conda config --system --add channels ics-conda-forge-virtual \
  && conda config --system --remove channels conda-e3 \
  && conda config --system --remove channels ics-conda \
  && conda config --system --remove channels anaconda-main \
  && conda config --system --remove channels conda-forge \
  && conda create -y -n pva-gateway p4p=${P4P_VERSION} \
  && conda clean -ay

WORKDIR /opt/pvagw

ENV PATH=/opt/conda/envs/pva-gateway/bin:$PATH

CMD ["/usr/local/bin/start.sh"]
