# pva-gateway

[Docker] image to run a [PVA Gateway](https://mdavidsaver.github.io/p4p/gw.html).

## Environment Variables

This image uses environment variables to create a simple gateway configuration.

Mandatory variables:

- `PVAGW_CLIENT_ADDRLIST`: List of broadcast and unicast addresses to which search messages will be sent
- `PVAGW_STATUSPREFIX`: Prefix for GW status PVs

Optional variables:

- `PVAGW_READONLY`: Boolean flag which acts as a global access control rule which rejects all PUT or RPC operations. This take precedence over any ACF file rules. [default: false]
- `CONTAINER_EXTERNAL_IP`: IP address to not use as server interface (other IP will be used)
- `PVAGW_SERVER_INTERFACE`: Local interface address to which this GW Server will bind [default: IP not matching the CONTAINER_EXTERNAL_IP]
- `PVAGW_SERVER_ADDRLIST`: List of broadcast and unicast addresses to which beacon messages will be sent [default: ""]
- `PVAGW_EXTRA_ARGUMENTS`: Arguments to pass to `pvagw`.

## How to use this image

Here is a basic docker-compose file showing how the image can be used (including a client).
To be useful the container should be attached to 2 networks.

```yaml
version: "2"
services:
  pvagw:
    image: registry.esss.lu.se/ics-docker/pva-gateway
    hostname: pvagw
    environment:
      CONTAINER_EXTERNAL_IP: 192.168.1.42
      PVAGW_CLIENT_ADDRLIST: 10.1.1.4
      PVAGW_STATUSPREFIX: "GW:STS:"
    expose:
      - "5075"
    networks:
      realnet:
        ipv4_address: 192.168.1.42
      jupyterhub:
  client:
    image: registry.esss.lu.se/ics-docker/epics-base
    command: sleep 1000000
    environment:
      EPICS_PVA_AUTO_ADDR_LIST: "no"
      EPICS_PVA_ADDR_LIST: "pvagw"
    networks:
      jupyterhub:
networks:
  jupyterhub:
  realnet:
    driver: macvlan
    driver_opts:
      parent: eth0
    ipam:
      config:
        - subnet: 192.168.1.0/24
          gateway: 192.168.1.1
          ip_range: 192.168.1.0/24
```

Note that to use a more complex configuration, you can mount the config file in the container and override the default command: `command: pvagw /opt/pvagw/mygw.conf`

[docker]: https://www.docker.com
